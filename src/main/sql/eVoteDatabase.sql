CREATE DATABASE IF NOT EXISTS eVote;
USE eVote;

DROP TABLE IF EXISTS Tokens;
DROP TABLE IF EXISTS PollsVotersLinks;
DROP TABLE IF EXISTS Voters;
DROP TABLE IF EXISTS Options;
DROP TABLE IF EXISTS Polls;



CREATE TABLE Polls (
    Name    VARCHAR(64),
    Type    VARCHAR(16),
    BegDate DATETIME,
    EndDate DATETIME,
    State   VARCHAR(6),
    PRIMARY KEY (Name)
);

CREATE TABLE Options (
    PollName   VARCHAR(64),
    OptionID   VARCHAR(64),
    Type       VARCHAR(16),
    Data       JSON,
    VotesCount INTEGER,
    PRIMARY KEY (PollName, OptionID)
);

CREATE TABLE Voters (
    PNC          VARCHAR(15),
    SeriesIC     VARCHAR(2)  NOT NULL,
    NumberIC     VARCHAR(6)  NOT NULL,
    IssuedBy     VARCHAR(64) NOT NULL,
    ExpDate      DATE        NOT NULL,
    NameF        VARCHAR(32) NOT NULL,
    NameL        VARCHAR(32) NOT NULL,
    BirthDate    DATE,
    Sex          VARCHAR(1),
    Citizenship  VARCHAR(2) DEFAULT "RO",
    PlaceOfBirth VARCHAR(128),
    CrtAddress   VARCHAR(128),
    PRIMARY KEY (PNC)
);

CREATE TABLE PollsVotersLinks (
    PollName VARCHAR(64),
    VoterPNC VARCHAR(15),
    PRIMARY KEY (PollName, VoterPNC)
);

CREATE TABLE Tokens (
    TokenID             INT,
    IssuerParametersUID TINYBLOB,
    PublicKey           TINYBLOB,
    TokenInformation    TINYBLOB,
    ProverInformation   TINYBLOB,
    SigmaZ              TINYBLOB,
    SigmaC              TINYBLOB,
    SigmaR              TINYBLOB,
    IsDeviceProtected   BOOLEAN DEFAULT FALSE,
    Message             TINYBLOB,
    DisclosedAttributes BLOB,
    A                   TINYBLOB,
    R0                  TINYBLOB,
    R                   BLOB,
    Rd                  TINYBLOB DEFAULT NULL,
    PRIMARY KEY (TokenID)
);



CREATE TRIGGER InsertBirthDateUsingPNC
BEFORE INSERT ON Voters
FOR EACH ROW
SET NEW.BirthDate = CONCAT(SUBSTRING(NEW.PNC, 2, 4), "-", SUBSTRING(NEW.PNC, 6, 2), "-", SUBSTRING(NEW.PNC, 8, 2));

CREATE TRIGGER InsertSexUsingPNC
BEFORE INSERT ON Voters
FOR EACH ROW
SET NEW.Sex = IF(SUBSTRING(NEW.PNC, 1, 1) = 1, "M", IF(substring(NEW.PNC, 1, 1) = 2, "F", NULL));



DROP PROCEDURE IF EXISTS AddPoll;
CREATE PROCEDURE AddPoll
(
    IN name    VARCHAR(64),
    IN type    VARCHAR(16),
    IN begDate DATETIME,
    IN endDate DATETIME,
    IN state   VARCHAR(6)
)
INSERT INTO Polls VALUE (name, type, begDate, endDate, state);


DROP PROCEDURE IF EXISTS RemPoll;
DELIMITER //
CREATE PROCEDURE RemPoll
(
    IN name VARCHAR(64)
)
BEGIN
    DELETE P, O, L FROM Polls P
    LEFT JOIN Options          O ON P.Name = O.PollName
    LEFT JOIN PollsVotersLinks L ON P.Name = L.PollName
    WHERE P.Name = name;
END //
DELIMITER ;


DROP PROCEDURE IF EXISTS UpdState;
CREATE PROCEDURE UpdState(IN pollName VARCHAR(64), IN state VARCHAR(6))
UPDATE Polls SET State = state WHERE Polls.Name = pollName;


DROP PROCEDURE IF EXISTS AddOption;
DELIMITER //
CREATE PROCEDURE AddOption
(
    IN pollName   VARCHAR(64),
    IN type       VARCHAR(16),
    IN data       JSON,
    IN votesCount INTEGER
)
BEGIN
    DECLARE optionID VARCHAR(64);

    IF (type = "A") THEN SELECT JSON_UNQUOTE(JSON_EXTRACT(data, "$.Text")) INTO optionID; END IF;
    IF (type = "C") THEN
        SELECT CONCAT(
            JSON_UNQUOTE(JSON_EXTRACT(data, "$.NameF")), " ",
            JSON_UNQUOTE(JSON_EXTRACT(data, "$.NameL"))
        ) INTO optionID;
    END IF;

    INSERT INTO Options VALUE (pollName, optionID, type, data, votesCount);
END //
DELIMITER ;


DROP PROCEDURE IF EXISTS RemOption;
CREATE PROCEDURE RemOption
(
    IN pollName VARCHAR(64),
    IN optionID VARCHAR(64)
)
DELETE FROM Options WHERE Options.PollName = pollName AND Options.OptionID = optionID;


DROP PROCEDURE IF EXISTS VerifyVoter;
DELIMITER //
CREATE PROCEDURE VerifyVoter
(
    IN  pnc         VARCHAR(15),
    IN  seriesIC    VARCHAR(2),
    IN  numberIC    VARCHAR(6),
    IN  issuedBy    VARCHAR(64),
    IN  expDate     DATE,
    IN  nameF       VARCHAR(64),
    IN  nameL       VARCHAR(64),
    IN  birthDate   DATE,
    IN  sex         VARCHAR(1),
    IN  citizenship VARCHAR(2),
    OUT isValid     BOOLEAN
)
BEGIN
    IF NOT EXISTS (
        SELECT * FROM Voters WHERE
        Voters.PNC         = pnc       AND
        Voters.SeriesIC    = seriesIC  AND
        Voters.NumberIC    = numberIC  AND
        Voters.IssuedBy    = issuedBy  AND
        Voters.ExpDate     = expDate   AND
        Voters.NameF       = nameF     AND
        Voters.NameL       = nameL     AND
        Voters.BirthDate   = birthDate AND
        Voters.Sex         = sex       AND
        Voters.Citizenship = citizenship
    )
    THEN SET isValid = FALSE;
    ELSE SET isValid =  TRUE;
    END IF;
END //
DELIMITER ;


DROP PROCEDURE IF EXISTS MarkAsVoted;
DELIMITER //
CREATE PROCEDURE MarkAsVoted
(
    IN  pollName VARCHAR(64),
    IN  voterPNC VARCHAR(15),
    OUT canVote  BOOLEAN
)
BEGIN
    IF EXISTS (SELECT * FROM PollsVotersLinks WHERE PollsVotersLinks.PollName = pollName AND PollsVotersLinks.VoterPNC = voterPNC)
    THEN SET canVote = FALSE;
    ELSE SET canVote =  TRUE;
    END IF;
    IF (canVote = TRUE) THEN INSERT INTO PollsVotersLinks VALUE (pollName, voterPNC); END IF;
END //
DELIMITER ;


DROP PROCEDURE IF EXISTS MarkToken;
DELIMITER //
CREATE PROCEDURE MarkToken
(
    IN  tokenID             INT,
    IN  issuerParametersUID TINYBLOB,
    IN  publicKey           TINYBLOB,
    IN  tokenInformation    TINYBLOB,
    IN  proverInformation   TINYBLOB,
    IN  sigmaZ              TINYBLOB,
    IN  sigmaC              TINYBLOB,
    IN  sigmaR              TINYBLOB,
    IN  message             TINYBLOB,
    IN  disclosedAttributes BLOB,
    IN  a                   TINYBLOB,
    IN  r0                  TINYBLOB,
    IN  r                   BLOB,
    OUT isValid             BOOLEAN
)
BEGIN
    IF EXISTS (SELECT * FROM Tokens WHERE Tokens.TokenID = tokenID)
    THEN SET isValid = FALSE;
    ELSE SET isValid =  TRUE;
    END IF;

    IF (isValid = TRUE) THEN
        INSERT INTO Tokens (TokenID, IssuerParametersUID, PublicKey, TokenInformation, ProverInformation, SigmaZ, SigmaC, SigmaR, Message, DisclosedAttributes, A, R0, R)
        VALUE              (tokenID, issuerParametersUID, publicKey, tokenInformation, proverInformation, sigmaZ, sigmaC, sigmaR, message, disclosedAttributes, a, r0, r);
    END IF;
END //
DELIMITER ;


DROP PROCEDURE IF EXISTS UpdateToken;
DELIMITER //
CREATE PROCEDURE UpdateToken
(
    IN  tokenID             INT,
    IN  message             TINYBLOB,
    IN  disclosedAttributes BLOB,
    IN  a                   TINYBLOB,
    IN  r0                  TINYBLOB,
    IN  r                   BLOB,
    OUT isValid             BOOLEAN
)
BEGIN
    IF NOT EXISTS (SELECT * FROM Tokens WHERE Tokens.TokenID = tokenID)
    THEN SET isValid = FALSE;
    ELSE SET isValid =  TRUE;
    END IF;
    IF (isValid = TRUE) THEN UPDATE Tokens SET Message = message, DisclosedAttributes = disclosedAttributes, A = a, R0 = r0, R = r WHERE Tokens.TokenID = tokenID; END IF;
END //
DELIMITER ;


DROP PROCEDURE IF EXISTS CountVotes;
DELIMITER //
CREATE PROCEDURE CountVotes
(
    IN  pollName   VARCHAR(64),
    IN  optionID   VARCHAR(64),
    OUT votesCount INTEGER
)
BEGIN
    SET votesCount = (SELECT count(*) FROM Tokens WHERE Tokens.TokenInformation = pollName AND Tokens.Message = optionID);

    UPDATE Options SET VotesCount = votesCount WHERE Options.PollName = pollName AND Options.OptionID = optionID;
END //
DELIMITER ;