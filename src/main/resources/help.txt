|
|-> help            prints this
|
|-> addpoll         creates a poll
|
|-> detpoll         prints details about a poll
|
|-> selpoll         selects a poll
|
    |-> detthis     prints details about the current poll
    |
    |-> addoptn     creates an option (REQUIRED POLL STATE(S): CONFIG)
    |
    |-> detoptn     prints details about an option
    |
    |-> remoptn     removes an option (REQUIRED POLL STATE(S): CONFIG)
    |
    |-> listoptns   prints the list of options
    |
    |-> update      updates the votes counting (REQUIRED POLL STATE(S): OPENED)
    |
    |-> reveal      reveals the votes counting (REQUIRED POLL STATE(S): CLOSED)
    |
    |-> next        advances the poll to the next state (REQUIRED POLL STATE(S): CONFIG, OPENED)
    |
    |-> back        returns to the poll center
|
|-> rempoll         removes a poll (REQUIRED POLL STATE(S): CONFIG)
|
|-> listpolls       prints the list of polls
|