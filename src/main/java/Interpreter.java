import core.InterpreterExecutor;

import java.io.*;
import java.net.Socket;

public class Interpreter implements Runnable
{
    private Socket socket;

    // receive data
    // from  client
    private DataInputStream recvData;

    // send data
    // to client
    private DataOutputStream sendData;



    @SuppressWarnings("FieldCanBeLocal")
    private int    cmdLength;
    private byte[] cmdBuffer;

    private String getCmd() throws IOException
    {
        cmdLength = recvData.readInt();

        recvData.readFully(cmdBuffer, 0, cmdLength);

        return new String(cmdBuffer, 0, cmdLength);
    }

    private void putRes(String res) throws IOException
    {
        sendData.writeInt(res.length());

        sendData.write(res.getBytes());
    }



    Interpreter(Socket socket)
    {
        cmdBuffer = new byte[8192];

        this.socket = socket;
        logging.LogManager.getInstance().logOpenedConnection(
                socket.getInetAddress().getHostAddress(),
                socket.getPort()
        );

        try {
            recvData = new DataInputStream (socket.getInputStream() );
            sendData = new DataOutputStream(socket.getOutputStream());
        } catch (IOException e) {
            util.CloseSocketLock.closeSocket(socket);
        }
    }



    @Override
    public void run()
    {
        try {
            var interpreterExecutor = new InterpreterExecutor();

            String cmd = "";
            String res = "";
            while (!cmd.equals("exit")) {
                cmd = getCmd();
                logging.LogManager.getInstance().logRecvData(
                        socket.getInetAddress().getHostAddress(),
                        socket.getPort(),
                        cmd
                );

                if (!cmd.equals("exit")) {
                    res = interpreterExecutor.executeCommand(cmd);

                    putRes(res);
                    logging.LogManager.getInstance().logSentData(
                            socket.getInetAddress().getHostAddress(),
                            socket.getPort(),
                            res
                    );
                }
            }
        } catch (IOException ignored) {
        } finally {
            util.CloseSocketLock.closeSocket(socket);
        }
    }
}
