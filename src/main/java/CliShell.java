import core.CliShellExecutor;

import java.io.IOException;

import static org.fusesource.jansi.Ansi.*;
import static org.fusesource.jansi.Ansi.Color.*;

public class CliShell implements Runnable
{
    private static final String incorrectCommand
            = ansi().fg(RED).a("Incorrect command! Type 'help' to see the list of commands.").reset().toString();

    private util.RunningState runningState;


    CliShell(util.RunningState runningState)
    {
        this.runningState = runningState;
    }


    @Override
    public void run()
    {
        java.util.Scanner in = new java.util.Scanner(System.in);


        String cmd = "help";
        while (!cmd.equals("exit")) {
            if (cmd.equals("help")) {
                util.Helper.getInstance().displayHelp();
            } else {
                if (CliShellExecutor.getInstance().checkCommand  (cmd)) {
                    CliShellExecutor.getInstance().executeCommand(cmd);
                } else System.out.println(incorrectCommand);
            }
            CliShellExecutor.getInstance().printPathCTX();

            cmd = in.nextLine();
        }


        try {
            runningState.shutDown();
        } catch (IOException e) {
            System.out.print(ansi().fg(RED));
            System.out.println("ERROR > " + e.getMessage());
            System.out.print(ansi().reset());
        }
    }
}
