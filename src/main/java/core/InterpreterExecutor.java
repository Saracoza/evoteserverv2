package core;

import authentication.*;
import data.*;
import electoral.*;
import uprove.converters.*;

public class InterpreterExecutor
{
    private static final String illegalOperationError = "Illegal operation!";

    @SuppressWarnings("FieldCanBeLocal")
    private final String emptyCommand = "";

    private uprove.controllers.AuthorityController authorityController;

    private String[] currentCTX;
    private String[] noArgsCMDs;

    private PollCenter pollCTR;
    private Poll       crtPoll;
    private String     pathCTX;



    public InterpreterExecutor()
    {
        authorityController = new uprove.controllers.AuthorityController();

        currentCTX = InspectionHelper.getInstance().getDefaultCTX();
        noArgsCMDs = InspectionHelper.getInstance().getNoArgsCMDs();

        pollCTR = PollCenter.getInstance();
        pathCTX = "Poll Center > ";
    }



    private boolean checkCommand(String cmd)
    {
        for (var currentCmd : currentCTX) {
            if (currentCmd.equals(cmd)) {
                return true;
            }
        }
        return false;
    }

    private boolean checkPattern(String cmd, String arg)
    {
        for (var currentCmd : noArgsCMDs) {
            if (currentCmd.equals(cmd)) {
                return arg.isEmpty();
            }
        }
        return true;
    }



    public String executeCommand(String command)
    {
        String response = "";
        try {
            var cmdAndArg = communication.Deserializer.deserializeCommand(command);
            var cmd = cmdAndArg.getValue0();
            var arg = cmdAndArg.getValue1();


            if (!checkCommand(cmd     )) throw new Exception("Incorrect command!");
            if (!checkPattern(cmd, arg)) throw new Exception("Incorrect pattern!");


            switch (cmd) {
                case "auth": {
                    if (!crtPoll.isOpened()) throw new Exception(illegalOperationError);

                    var authObj = AuthHelper.deserializeAuthObj(arg);

                    var date1 = new String(authObj[4]);
                    var date2 = new String(authObj[7]);
                    if (!date1.matches("([12]\\d{3}-(0[1-9]|1[0-2])-(0[1-9]|[12]\\d|3[01]))") ||
                        !date2.matches("([12]\\d{3}-(0[1-9]|1[0-2])-(0[1-9]|[12]\\d|3[01]))")) {
                        throw new Exception("Authentication failed!");
                    }

                    if (!DataAccess.getInstance().verifyVoter(
                            new String(authObj[0]),
                            new String(authObj[1]),
                            new String(authObj[2]),
                            new String(authObj[3]),
                            java.sql.Date.valueOf(date1),
                            new String(authObj[5]),
                            new String(authObj[6]),
                            java.sql.Date.valueOf(date2),
                            new String(authObj[8]),
                            new String(authObj[9])
                    )) throw new Exception("Authentication failed!");

                    if (!data.DataAccess.getInstance().markAsVoted(crtPoll.getID(), new String(authObj[0]))) {
                        throw new Exception(
                                "Server could not mark you as voted."
                                + System.lineSeparator() +
                                "Make sure you've not already voted!"
                        );
                    }

                    authorityController.initialize(crtPoll.getID().getBytes(), authObj);

                    return communication.Serializer.serializeResponse(communication.ResponseType.Success, "Authentication succeeded...");
                }

                case "vote": {
                    var messageTokenProof = Deserializer.deserializeMessageTokenProof(arg);
                    var message = messageTokenProof.getValue0();
                    var token   = messageTokenProof.getValue1();
                    var proof   = messageTokenProof.getValue2();
                    authorityController.verifyProof(message, token, proof);

                    if (!pollCTR.getPoll(new String(token.getTokenInformation())).isOpened()) throw new Exception(illegalOperationError);

                    if (!DataAccess.getInstance().markToken(
                            token.hashCode(),
                            token.getIssuerParametersUID(),
                            token.getPublicKey(),
                            token.getTokenInformation(),
                            token.getProverInformation(),
                            token.getSigmaZ(),
                            token.getSigmaC(),
                            token.getSigmaR(),
                            message,
                            proof.getDisclosedAttributes(),
                            proof.getA(),
                            proof.getR0(),
                            proof.getR()
                    )) throw new Exception(
                            "Server could not register your token."
                            + System.lineSeparator() +
                            "Make sure you've not already used it!"
                    );

                    return communication.Serializer.serializeResponse(communication.ResponseType.Success, "Your vote was registered successfully!");
                }

                case "updvote": {
                    var messageTokenProof = Deserializer.deserializeMessageTokenProof(arg);
                    var message = messageTokenProof.getValue0();
                    var token   = messageTokenProof.getValue1();
                    var proof   = messageTokenProof.getValue2();
                    authorityController.verifyProof(message, token, proof);

                    if (!pollCTR.getPoll(new String(token.getTokenInformation())).isOpened()) throw new Exception(illegalOperationError);

                    if (!DataAccess.getInstance().UpdateToken(
                            token.hashCode(),
                            message,
                            proof.getDisclosedAttributes(),
                            proof.getA(),
                            proof.getR0(),
                            proof.getR()
                    )) throw new Exception(
                            "Server could not update your token."
                            + System.lineSeparator() +
                            "Make sure  you've already  used it!"
                    );

                    return communication.Serializer.serializeResponse(communication.ResponseType.Success, "Your vote was updated successfully!");
                }

                case "init": { response = Serializer.serializeIssuerParameters(uprove.storage.AuthoritySettings.getInstance().getIssuerParameters()); } break;

                case "genmsg1": { response = Serializer.bytesToBase64(authorityController.generateFirstMessage());                                } break;
                case "genmsg3": { response = Serializer.bytesToBase64(authorityController.generateThirdMessage(Deserializer.base64ToBytes(arg))); } break;

                case "detpoll": { response = pollCTR.getPoll(arg).getDetails();   } break;
                case "detthis": { response = crtPoll.getDetails();                } break;
                case "detoptn": { response = crtPoll.getOption(arg).getDetails(); } break;

                case "selpoll": { if (!pollCTR.getPoll(arg).isConfig()) pathCTX += (crtPoll = pollCTR.getPoll(arg)).getID() + " > "; else throw new Exception(illegalOperationError); } break;

                case "back": { pathCTX = "Poll Center > "; } break;

                case "listpolls": { response = pollCTR.listPolls();   } break;
                case "listoptns": { response = crtPoll.listOptions(); } break;

                case "getpathctx": { response = pathCTX;                 } break;
                case "getselpoll": { response = crtPoll.getID();         } break;
                case "getoptions": { response = crtPoll.getOptionsIDs(); } break;

                case "reveal": { if (crtPoll.isClosed()) response = crtPoll.getResults(); else throw new Exception(illegalOperationError); } break;
            }


            currentCTX = cmd.equals(emptyCommand) ||
                         cmd.equals("getpathctx")  ?
                         currentCTX :
                         InspectionHelper.getInstance().getCTX(cmd).split(":");

        } catch (Exception e) {
            return communication.Serializer.serializeResponse(communication.ResponseType.Error, e.getMessage());
        }
        return communication.Serializer.serializeResponse(communication.ResponseType.OK, response);
    }
}
