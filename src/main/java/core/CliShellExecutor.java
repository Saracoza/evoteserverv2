package core;

import data.*;
import electoral.*;

import java.io.IOException;
import java.util.Hashtable;

import static org.fusesource.jansi.Ansi.*;
import static org.fusesource.jansi.Ansi.Color.*;

public class CliShellExecutor
{
    private static final String illegalOperationErr = "Illegal operation!";

    private final String optionTypeQuery = ansi().fg(CYAN).a("Option type ('A' -> answer, 'C' -> candidate): ").reset().toString();
    private final String colorPollName   = ansi().fg(CYAN).a("Poll name: ").reset().toString();
    private final String colorOptionID   = ansi().fg(CYAN).a("Option ID: ").reset().toString();
    private final String defaultPath     = ansi().bgBright(WHITE).fg(BLUE).a("Poll Center").reset().toString();

    private java.util.Scanner in = new java.util.Scanner(System.in);

    private java.util.function.BiFunction<String, String, Integer> countingFunction;



    private Hashtable<String, String> clishellctx = new Hashtable<>();

    private String[] currentCTX;



    private DataAccess dataCTR;
    private PollCenter pollCTR;
    private Poll       crtPoll;
    private String     pathCTX;



    private CliShellExecutor() throws IOException
    {
        dataCTR = DataAccess.getInstance();
        pollCTR = PollCenter.getInstance();
        pathCTX = defaultPath + " > ";
        countingFunction = (pollName, optionID) -> dataCTR.CountVotes(pollName, optionID);



        var fileReader = new java.io.FileReader(util.FilesManager.getInstance().getFilePath("clishellctx.dat"));
        var buffReader = new java.io.BufferedReader(fileReader);

        String   lineBuffer;
        String[] clishellctxBuf;

        while ((lineBuffer = buffReader.readLine()) != null) {
            clishellctxBuf = lineBuffer.split("\\p{javaWhitespace}+::", 2);
            clishellctx.put(
                    clishellctxBuf[0],
                    clishellctxBuf[1]
            );
        }

        fileReader.close();
        buffReader.close();

        currentCTX = clishellctx.get("default").split(":");
    }



    private static CliShellExecutor instance;
    public  static void createInstance() throws IOException
    {
        if (instance == null) {
            instance = new CliShellExecutor();
        }
    }
    public  static CliShellExecutor getInstance()
    {
        return instance;
    }



    public boolean checkCommand(String cmd)
    {
        for (String command : currentCTX) {
            if (cmd.equals(command)) {
                return true;
            }
        }
        return false;
    }

    public void executeCommand(String cmd)
    {
        try {
            switch (cmd) {
                case "addpoll": {
                    var poll = ElectoralFactory.buildPoll();
                    if (poll != null) {
                        pollCTR.addPoll(poll);
                        dataCTR.addPoll(
                                poll.getID(),
                                poll.getType().toString(),
                                new java.sql.Timestamp(poll.getBegDate().getTime()),
                                new java.sql.Timestamp(poll.getEndDate().getTime()),
                                poll.getState().toString()
                        );
                    }
                } break;
                case "detpoll": {
                    System.out.print(colorPollName);
                    String pollName = in.nextLine();
                    System.out.println(pollCTR.getPoll(pollName).getDetails());
                } break;
                case "rempoll": {
                    System.out.print(colorPollName);
                    String pollName = in.nextLine();
                    pollCTR.remPoll(pollName);
                    dataCTR.remPoll(pollName);
                } break;
                case "selpoll": {
                    System.out.print(colorPollName);
                    String pollName = in.nextLine();
                    crtPoll = pollCTR.getPoll(pollName);

                    pathCTX = defaultPath
                            + " > "
                            + ansi().bg(YELLOW).fg(BLACK).a(pollName).reset()
                            + " > ";
                } break;

                case "detthis": { System.out.println(crtPoll.getDetails()); } break;

                case "addoptn": {
                    System.out.print(optionTypeQuery);
                    String optionType = in.nextLine();

                    Option option;
                    switch (optionType) {
                        case "A": { option = ElectoralFactory.buildAnswer();    } break;
                        case "C": { option = ElectoralFactory.buildCandidate(); } break;
                        default:  { throw new Exception("Incorrect type format! Option could not be created."); }
                    }

                    if (option != null) {
                        crtPoll.addOption(option);
                        dataCTR.addOption(
                                crtPoll.getID(),
                                optionType,
                                option.getData(),
                                option.getVotesCount()
                        );
                    }
                } break;
                case "detoptn": {
                    System.out.print(colorOptionID);
                    String optionID = in.nextLine();
                    System.out.println(crtPoll.getOption(optionID).getDetails());
                } break;
                case "remoptn": {
                    System.out.print(colorOptionID);
                    String optionID = in.nextLine();
                    crtPoll.remOption(optionID);
                    dataCTR.remOption(crtPoll.getID(), optionID);
                } break;

                case "update": { if (crtPoll.isOpened()) crtPoll.updateCounting(countingFunction); else throw new Exception(illegalOperationErr); } break;
                case "reveal": { if (crtPoll.isClosed()) System.out.println(crtPoll.getResults()); else throw new Exception(illegalOperationErr); } break;

                case "next": {
                    if (crtPoll.isClosed()) throw new Exception(illegalOperationErr);
                    dataCTR.updState(crtPoll.getID(), crtPoll.nxtState().toString());
                    if (crtPoll.isClosed()) crtPoll.updateCounting(countingFunction);
                } break;

                case "back": { pathCTX = defaultPath + " > "; } break;

                case "listpolls": { System.out.println(pollCTR.listPolls());   } break;
                case "listoptns": { System.out.println(crtPoll.listOptions()); } break;
            }

            currentCTX = cmd.equals("") ? currentCTX : clishellctx.get(cmd).split(":");
        } catch (Exception e) {
            System.out.print(ansi().fg(RED));
            System.out.println(e.getMessage());
            System.out.print(ansi().reset());
        }
    }



    public void printPathCTX()
    {
        System.out.print(pathCTX);
    }
}
