package core;

import java.io.IOException;
import java.util.Hashtable;

public class InspectionHelper
{
    private Hashtable<String, String> cmdctx = new Hashtable<>();



    private InspectionHelper() throws IOException
    {
        var fileReader = new java.io.FileReader(util.FilesManager.getInstance().getFilePath("cmdctx.dat"));
        var buffReader = new java.io.BufferedReader(fileReader);

        String   lineBuff;
        String[] cmdctxBuffer;

        while ((lineBuff = buffReader.readLine()) != null) {
            cmdctxBuffer = lineBuff.split("\\p{javaWhitespace}+::", 2);
            cmdctx.put(
                    cmdctxBuffer[0],
                    cmdctxBuffer[1]
            );
        }

        fileReader.close();
        buffReader.close();
    }



    private static InspectionHelper instance;
    public  static void createInstance() throws IOException
    {
        if (instance == null) {
            instance = new InspectionHelper();
        }
    }
    public  static InspectionHelper getInstance()
    {
        return instance;
    }



    public String[] getDefaultCTX()
    {
        return cmdctx.get("default").split(":");
    }

    public String getCTX(String command)
    {
        return cmdctx.get(command);
    }



    private String[] noArgsCMDs = new String[] {"", "genmsg1", "detthis", "back", "listpolls", "listoptns", "getpathctx", "getselpoll", "getoptions"};
    public  String[] getNoArgsCMDs()
    {
        return noArgsCMDs;
    }
}
