package electoral;

public abstract class Option implements ElectoralObject
{
    protected int votesCount = 0;


    public int  getVotesCount()               { return votesCount;            }
    public void setVotesCount(int votesCount) { this.votesCount = votesCount; }


    public abstract String getData();
}
