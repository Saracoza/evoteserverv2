package electoral;

import java.text.DecimalFormat;
import java.util.Date;

public class Poll implements ElectoralObject
{
    private static final String illegalOperationError = "Illegal operation!";

    private static final String addOptionError1 = "Option already exists!";
    private static final String addOptionError2 =
            "Can`t add:"
            + System.lineSeparator() + "- candidate to referendum"
            + System.lineSeparator() + "- answer    to   election";

    private static final String noOptionError = "Option does not exist!";

    private java.util.Hashtable<String, Option> options = new java.util.Hashtable<>();



    private String    name;
    private PollType  type;
    private Date      begDate;
    private Date      endDate;
    private PollState state;

    public Poll(String name, PollType type, Date begDate, Date endDate, PollState state)
    {
        this.name    = name;
        this.type    = type;
        this.begDate = begDate;
        this.endDate = endDate;
        this.state   = state;
    }

    public PollType getType()
    {
        return type;
    }

    public Date getBegDate()
    {
        return begDate;
    }

    public Date getEndDate()
    {
        return endDate;
    }

    public PollState getState()
    {
        return state;
    }



    @Override
    public String getID()
    {
        return name;
    }

    @Override
    public String getDetails()
    {
        return "Name: " + name + System.lineSeparator() +
               "Type: " + type + System.lineSeparator() +
               "Start date: " + begDate.toString() + System.lineSeparator() +
               "Close date: " + endDate.toString() + System.lineSeparator() +
               "######" + System.lineSeparator() + state.toString().toUpperCase();
    }



    public void addOption(Option option, boolean bypassChecks) throws Exception
    {
        if (!bypassChecks && !isConfig()) throw new Exception(illegalOperationError);


        if ((type == PollType.Referendum && option instanceof Answer   ) ||
             type == PollType.Election   && option instanceof Candidate)  {

            if (options.containsKey(option.getID())) throw new Exception(addOptionError1);
            else {
                options.put(
                        option.getID(),
                        option
                );
            }
        } else throw new Exception(addOptionError2);
    }



    public void addOption(Option option) throws Exception
    {
        addOption(option, false);
    }

    public void remOption(String optionID) throws Exception
    {
        if (!isConfig()) throw new Exception(illegalOperationError);

        options.remove(optionID);
    }

    public Option getOption(String optionID) throws Exception
    {
        if (options.containsKey(optionID)) return options.get(optionID);
        else                               throw new Exception(noOptionError);
    }



    public String listOptions()
    {
        StringBuilder stringBuilder = new StringBuilder();

        stringBuilder.append("+--");
        stringBuilder.append(System.lineSeparator());
        options.forEach((optionID, option) -> stringBuilder.append("| # ").append(optionID).append(System.lineSeparator()));
        stringBuilder.append("+--");

        return stringBuilder.toString();
    }

    public String getOptionsIDs()
    {
        StringBuilder stringBuilder = new StringBuilder();

        options.forEach((optionID, option) -> stringBuilder.append(optionID).append("::"));
        stringBuilder.deleteCharAt(stringBuilder.length() - 1);
        stringBuilder.deleteCharAt(stringBuilder.length() - 1);

        return stringBuilder.toString();
    }



    public PollState nxtState()
    {
        switch (state) {
            case Config: { state = PollState.Opened; } break;
            case Opened: { state = PollState.Closed; } break;
        }
        return state;
    }

    @SuppressWarnings("BooleanMethodIsAlwaysInverted")
    public boolean isConfig()
    {
        return state.equals(PollState.Config);
    }

    public boolean isOpened()
    {
        return state.equals(PollState.Opened);
    }

    public boolean isClosed()
    {
        return state.equals(PollState.Closed);
    }



    public void updateCounting(java.util.function.BiFunction<String, String, Integer> countingFunction)
    {
        options.forEach((optionID, option) -> option.setVotesCount(countingFunction.apply(name, optionID)));
    }



    private String results;
    public  String getResults()
    {
        if (results == null) {
            DecimalFormat decimalFormat = new DecimalFormat("000,000,000");
            StringBuilder stringBuilder = new StringBuilder();

            stringBuilder.append("+-------------+-----------+").append(System.lineSeparator());
            stringBuilder.append("|    Votes    | Option ID |").append(System.lineSeparator());
            stringBuilder.append("+-------------+-----------+").append(System.lineSeparator());

            options.forEach((optionID, option) ->
                    stringBuilder
                            .append("  ")
                            .append(decimalFormat.format(option.getVotesCount()))
                            .append(" | ")
                            .append(optionID)
                            .append(System.lineSeparator()));

            stringBuilder.append("+-------------+-----------+");

            results = stringBuilder.toString();
        }
        return results;
    }
}
