package electoral;

interface ElectoralObject
{
    String getID();
    String getDetails();
}
