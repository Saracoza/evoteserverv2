package electoral;

import org.json.simple.JSONObject;

public class Candidate extends Option
{
    private String nameF;
    private String nameL;
    private String party;

    public Candidate(String nameF, String nameL, String party)
    {
        this.nameF = nameF;
        this.nameL = nameL;
        this.party = party;
    }



    @Override
    public String getID()
    {
        return nameF + " " + nameL;
    }

    @Override
    public String getDetails()
    {
        return "First name: " + nameF +
               System.lineSeparator() +
               "Last  name: " + nameL +
               System.lineSeparator() +
               "Party: " + party;
    }



    @Override
    @SuppressWarnings("unchecked")
    public String getData()
    {
        JSONObject candidateData = new JSONObject();
        candidateData.put("NameF", nameF);
        candidateData.put("NameL", nameL);
        candidateData.put("Party", party);
        return candidateData.toJSONString();
    }
}
