package electoral;

public enum PollState
{
    Config, Opened, Closed
}
