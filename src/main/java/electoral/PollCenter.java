package electoral;

public class PollCenter
{
    private static final String illegalOperationError = "Illegal operation!";

    private static final String addPollError = "Poll already exists!";
    private static final String noPollError  = "Poll does not exist!";

    private java.util.Hashtable<String, Poll> polls = new java.util.Hashtable<>();



    private PollCenter() {}
    private static PollCenter instance;
    public  static PollCenter getInstance()
    {
        if (instance == null) {
            instance = new PollCenter();
        }
        return instance;
    }



    public void addPoll(Poll poll) throws Exception
    {
        if (polls.containsKey(poll.getID())) throw new Exception(addPollError);
        else {
            polls.put(
                    poll.getID(),
                    poll
            );
        }
    }

    public void remPoll(String pollName) throws Exception
    {
        if (polls.containsKey(pollName) && !polls.get(pollName).isConfig()) throw new Exception(illegalOperationError);

        polls.remove(pollName);
    }

    public Poll getPoll(String pollName) throws Exception
    {
        if (polls.containsKey(pollName)) return polls.get(pollName);
        else                             throw new Exception(noPollError);
    }



    public String listPolls()
    {
        StringBuilder stringBuilder = new StringBuilder();

        stringBuilder.append("+---------+");
        stringBuilder.append(System.lineSeparator());

        polls.forEach((pollName, poll) ->
                stringBuilder
                        .append("|  ")
                        .append(poll.getState())
                        .append(" # ")
                        .append(pollName)
                        .append(System.lineSeparator()));

        stringBuilder.append("+---------+");

        return stringBuilder.toString();
    }
}
