package electoral;

import java.text.SimpleDateFormat;

import static org.fusesource.jansi.Ansi.*;
import static org.fusesource.jansi.Ansi.Color.*;

public class ElectoralFactory
{
    private static java.util.Scanner in = new java.util.Scanner(System.in);

    public static Poll buildPoll()
    {
        System.out.print(ansi().fg(YELLOW));
        System.out.println("Creating new poll...");
        System.out.println("--------------------");
        System.out.print(ansi().reset());

        System.out.print("Poll type ('r' -> referendum, 'e' -> election): ");
        String pollTypeBuffer = in.nextLine();

        var pollType = pollTypeBuffer.equals("r") ? PollType.Referendum : (pollTypeBuffer.equals("e") ? PollType.Election : null);
        if (pollType == null) {
            System.out.print(ansi().fg(RED));
            System.out.println("Incorrect type format! Poll could not be created.");
            System.out.print(ansi().reset());
            return null;
        }

        System.out.print("Poll name: ");
        String pollName = in.nextLine();
        if (pollName.isEmpty()) {
            System.out.print(ansi().fg(RED));
            System.out.println("Poll's name can't be empty!");
            System.out.println("Poll  could not be created.");
            System.out.print(ansi().reset());
            return null;
        }

        var dateFormat = new SimpleDateFormat("yyyy.MM.dd hh:mm:ss a");
        System.out.println("Poll starting date (format: yyyy.mm.dd hh:mm:ss AM/PM):");
        var begDateBuffer = in.nextLine();
        System.out.println("Poll ending   date (format: yyyy.mm.dd hh:mm:ss AM/PM):");
        var endDateBuffer = in.nextLine();

        Poll poll = null;
        try {
            var pollBegDate = dateFormat.parse(begDateBuffer);
            var pollEndDate = dateFormat.parse(endDateBuffer);

            poll = new Poll(pollName, pollType, pollBegDate, pollEndDate, PollState.Config);

            System.out.print(ansi().fg(GREEN));
            System.out.println("--------------------------");
            System.out.println("Poll created successfully!");
            System.out.print(ansi().reset());
        } catch (Exception e) {
            System.out.print(ansi().fg(RED));
            System.out.println("Incorrect date format! Poll could not be created.");
            System.out.print(ansi().reset());
        }

        return poll;
    }

    public static Answer buildAnswer()
    {
        System.out.print(ansi().fg(YELLOW));
        System.out.println("Creating new answer...");
        System.out.println("----------------------");
        System.out.print(ansi().reset());

        System.out.print("Answer text: ");
        var answerText = in.nextLine();
        if (answerText.isEmpty()) {
            System.out.print(ansi().fg(RED));
            System.out.println("Answer's text can't be empty!");
            System.out.println("Answer  could not be created.");
            System.out.print(ansi().reset());
            return null;
        }

        System.out.print("Answer info: ");
        String answerInfo = in.nextLine();

        System.out.print(ansi().fg(GREEN));
        System.out.println("----------------------------");
        System.out.println("Answer created successfully!");
        System.out.print(ansi().reset());
        return new Answer(answerText, answerInfo);
    }

    public static Candidate buildCandidate()
    {
        System.out.print(ansi().fg(YELLOW));
        System.out.println("Creating new candidate...");
        System.out.println("-------------------------");
        System.out.print(ansi().reset());

        System.out.print("Candidate`s first name: ");
        String nameBuffer1 = in.nextLine();
        System.out.print("Candidate`s  last name: ");
        String nameBuffer2 = in.nextLine();

        if (nameBuffer1.isEmpty() || nameBuffer2.isEmpty()) {
            System.out.print(ansi().fg(RED));
            System.out.println("Candidate's name can't be empty!");
            System.out.println("Candidate  could not be created.");
            System.out.print(ansi().reset());
            return null;
        }

        System.out.print("Candidate`s party: ");
        String party = in.nextLine();

        System.out.print(ansi().fg(GREEN));
        System.out.println("-------------------------------");
        System.out.println("Candidate created successfully!");
        System.out.print(ansi().reset());
        return new Candidate(nameBuffer1, nameBuffer2, party);
    }
}
