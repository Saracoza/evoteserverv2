package electoral;

import org.json.simple.JSONObject;

public class Answer extends Option
{
    private String text;
    private String info;

    public Answer(String text, String info)
    {
        this.text = text;
        this.info = info;
    }



    @Override
    public String getID()
    {
        return text;
    }

    @Override
    public String getDetails()
    {
        return "Text: " + text + System.lineSeparator() + "Info: " + info;
    }



    @Override
    @SuppressWarnings("unchecked")
    public String getData()
    {
        JSONObject answerData = new JSONObject();
        answerData.put("Text", text);
        answerData.put("Info", info);
        return answerData.toJSONString();
    }
}
