import java.net.ServerSocket;
import java.util.concurrent.Executors;

import org.fusesource.jansi.AnsiConsole;
import static org.fusesource.jansi.Ansi.*;
import static org.fusesource.jansi.Ansi.Color.*;

public class Main
{
    public static void main(String[] args)
    {
        try (var listener = new ServerSocket(Integer.parseInt(conf.SettingsChain.createInstance().getSettingValue("serverPort")))) {
            AnsiConsole.systemInstall();

            System.out.println(ansi().fg(YELLOW).a("Loading...").reset());
            util.Initializer.LoadComponents();
            System.out.println(ansi().fg(GREEN ).a("Running...").reset());

            // shared variable between threads
            // to control the state of running
            var runningState = new util.RunningState();

            // make a new thread for
            // running the CLI shell
            new Thread(new CliShell(runningState)).start();

            // limit the number of simultaneously connections by
            // making a thread pool for running the interpreters
            var threadPool = Executors.newFixedThreadPool(10);

            while (runningState.isRunning()) {
                var socket = listener.accept();
                runningState.addSocket(socket);
                threadPool.execute(new Interpreter(socket));
            }
            runningState.closeSockets();

            // main thread will not stop running
            // until the thread pool's shut down
            threadPool.shutdown();

        } catch (Exception e) {
            System.out.print(ansi().fg(RED));
            System.out.println("ERROR > " + e.getMessage());
            System.out.print(ansi().reset());
        } finally {
            AnsiConsole.systemUninstall();

            if (logging.LogManager.getInstance() != null) {
                logging.LogManager.getInstance().CloseLogFile();
            }
        }
    }
}
