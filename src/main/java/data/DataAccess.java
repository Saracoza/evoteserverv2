package data;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import javax.sql.rowset.serial.SerialBlob;

import java.sql.*;

import static org.fusesource.jansi.Ansi.*;
import static org.fusesource.jansi.Ansi.Color.*;

public class DataAccess
{
    private Connection connection;

    private DataAccess() throws SQLException
    {
        var settingsChainInstance = conf.SettingsChain.getInstance();

        String url = "jdbc:mysql://"
                + settingsChainInstance.getSettingValue("databaseIP")
                + ":"
                + settingsChainInstance.getSettingValue("databasePort")
                + "/"
                + settingsChainInstance.getSettingValue("databaseName");
        String username = settingsChainInstance.getSettingValue("databaseUsername");
        String password = settingsChainInstance.getSettingValue("databasePassword");

        connection = DriverManager.getConnection(url, username, password);
    }



    private static DataAccess instance;
    public  static void createInstance() throws SQLException
    {
        if (instance == null) {
            instance = new DataAccess();
        }
    }
    public  static DataAccess getInstance()
    {
        return instance;
    }



    public void addPoll(String name, String type, Timestamp begDate, Timestamp endDate, String state)
    {
        try (var callableStatement = connection.prepareCall("{call AddPoll(?, ?, ?, ?, ?)}")) {
            callableStatement.setString(1, name);
            callableStatement.setString(2, type);
            callableStatement.setTimestamp(3, begDate);
            callableStatement.setTimestamp(4, endDate);
            callableStatement.setString(5, state);

            callableStatement.execute();
        } catch (SQLException e) {
            System.out.print(ansi().fg(RED));
            System.out.println(System.lineSeparator() + "ERROR > " + e.getMessage());
            System.out.print(ansi().reset());
        }
    }

    public void remPoll(String name)
    {
        try (var callableStatement = connection.prepareCall("{call RemPoll(?)}")) {
            callableStatement.setString(1, name);

            callableStatement.execute();
        } catch (SQLException e) {
            System.out.print(ansi().fg(RED));
            System.out.println(System.lineSeparator() + "ERROR > " + e.getMessage());
            System.out.print(ansi().reset());
        }
    }

    public void updState(String pollName, String state)
    {
        try (var callableStatement = connection.prepareCall("{call UpdState(?, ?)}")) {
            callableStatement.setString(1, pollName);
            callableStatement.setString(2, state);

            callableStatement.execute();
        } catch (SQLException e) {
            System.out.print(ansi().fg(RED));
            System.out.println(System.lineSeparator() + "ERROR > " + e.getMessage());
            System.out.print(ansi().reset());
        }
    }

    public void addOption(String pollName, String type, String data, int votesCount)
    {
        try (var callableStatement = connection.prepareCall("{call AddOption(?, ?, ?, ?)}")) {
            callableStatement.setString(1, pollName);
            callableStatement.setString(2, type);
            callableStatement.setString(3, data);
            callableStatement.setInt(4, votesCount);

            callableStatement.execute();
        } catch (SQLException e) {
            System.out.print(ansi().fg(RED));
            System.out.println(System.lineSeparator() + "ERROR > " + e.getMessage());
            System.out.print(ansi().reset());
        }
    }

    public void remOption(String pollName, String optionID)
    {
        try (var callableStatement = connection.prepareCall("{call RemOption(?, ?)}")) {
            callableStatement.setString(1, pollName);
            callableStatement.setString(2, optionID);

            callableStatement.execute();
        } catch (SQLException e) {
            System.out.print(ansi().fg(RED));
            System.out.println(System.lineSeparator() + "ERROR > " + e.getMessage());
            System.out.print(ansi().reset());
        }
    }



    public boolean verifyVoter(
            String pnc,
            String seriesIC,
            String numberIC,
            String issuedBy,
            Date   expDate,
            String nameF,
            String nameL,
            Date   birthDate,
            String sex,
            String citizenship)
    {
        try (var callableStatement = connection.prepareCall("{call VerifyVoter(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)}")) {
            callableStatement.registerOutParameter(11, Types.BOOLEAN);

            callableStatement.setString (1,  pnc);
            callableStatement.setString (2,  seriesIC);
            callableStatement.setString (3,  numberIC);
            callableStatement.setString (4,  issuedBy);
            callableStatement.setDate   (5,  expDate);
            callableStatement.setString (6,  nameF);
            callableStatement.setString (7,  nameL);
            callableStatement.setDate   (8,  birthDate);
            callableStatement.setString (9,  sex);
            callableStatement.setString (10, citizenship);

            callableStatement.execute();
            return callableStatement.getBoolean(11);

        } catch (SQLException e) {
            System.out.print(ansi().fg(RED));
            System.out.println(System.lineSeparator() + "ERROR > " + e.getMessage());
            System.out.print(ansi().reset());

            return false;
        }
    }

    public boolean markAsVoted(String pollName, String voterPNC)
    {
        try (var callableStatement = connection.prepareCall("{call MarkAsVoted(?, ?, ?)}")) {
            callableStatement.registerOutParameter(3, Types.BOOLEAN);

            callableStatement.setString(1, pollName);
            callableStatement.setString(2, voterPNC);

            callableStatement.execute();
            return callableStatement.getBoolean(3);

        } catch (SQLException e) {
            System.out.print(ansi().fg(RED));
            System.out.println(System.lineSeparator() + "ERROR > " + e.getMessage());
            System.out.print(ansi().reset());

            return false;
        }
    }



    public boolean markToken(
            int tokenID,
            byte[] issuerParametersUID,
            byte[] publicKey,
            byte[] tokenInformation,
            byte[] proverInformation,
            byte[] sigmaZ,
            byte[] sigmaC,
            byte[] sigmaR,
            byte[] message,
            byte[][] disclosedAttributes, byte[] a, byte[] r0, byte[][] r)
    {
        try (var callableStatement = connection.prepareCall("{call MarkToken(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)}")) {
            callableStatement.registerOutParameter(14, Types.BOOLEAN);

            callableStatement.setInt(1, tokenID);

            callableStatement.setBlob(2, new SerialBlob(issuerParametersUID));
            callableStatement.setBlob(3, new SerialBlob(publicKey));
            callableStatement.setBlob(4, new SerialBlob(tokenInformation));
            callableStatement.setBlob(5, new SerialBlob(proverInformation));
            callableStatement.setBlob(6, new SerialBlob(sigmaZ));
            callableStatement.setBlob(7, new SerialBlob(sigmaC));
            callableStatement.setBlob(8, new SerialBlob(sigmaR));

            callableStatement.setBlob(9, new SerialBlob(message));

            callableStatement.setBlob(10, new SerialBlob(
                    uprove.converters.Serializer.bytesToBase64(
                            disclosedAttributes
                    ).getBytes()
            ));
            callableStatement.setBlob(11, new SerialBlob(a));
            callableStatement.setBlob(12, new SerialBlob(r0));
            callableStatement.setBlob(13, new SerialBlob(
                    uprove.converters.Serializer.bytesToBase64(
                            r
                    ).getBytes()
            ));

            callableStatement.execute();
            return callableStatement.getBoolean(14);

        } catch (SQLException e) {
            System.out.print(ansi().fg(RED));
            System.out.println(System.lineSeparator() + "ERROR > " + e.getMessage());
            System.out.print(ansi().reset());

            return false;
        }
    }

    public boolean UpdateToken(
            int tokenID,
            byte[] message,
            byte[][] disclosedAttributes, byte[] a, byte[] r0, byte[][] r)
    {
        try (var callableStatement = connection.prepareCall("{call UpdateToken(?, ?, ?, ?, ?, ?, ?)}")) {
            callableStatement.registerOutParameter(7, Types.BOOLEAN);

            callableStatement.setInt (1, tokenID);
            callableStatement.setBlob(2, new SerialBlob(message));
            callableStatement.setBlob(3, new SerialBlob(
                    uprove.converters.Serializer.bytesToBase64(
                            disclosedAttributes
                    ).getBytes()
            ));
            callableStatement.setBlob(4, new SerialBlob(a));
            callableStatement.setBlob(5, new SerialBlob(r0));
            callableStatement.setBlob(6, new SerialBlob(
                    uprove.converters.Serializer.bytesToBase64(
                            r
                    ).getBytes()
            ));

            callableStatement.execute();
            return callableStatement.getBoolean(7);

        } catch (SQLException e) {
            System.out.print(ansi().fg(RED));
            System.out.println(System.lineSeparator() + "ERROR > " + e.getMessage());
            System.out.print(ansi().reset());

            return false;
        }
    }



    public int CountVotes(String pollName, String optionID)
    {
        try (var callableStatement = connection.prepareCall("{call CountVotes(?, ?, ?)}")) {
            callableStatement.registerOutParameter(3, Types.INTEGER);

            callableStatement.setString(1, pollName);
            callableStatement.setString(2, optionID);

            callableStatement.execute();
            return callableStatement.getInt(3);

        } catch (SQLException e) {
            System.out.print(ansi().fg(RED));
            System.out.println(System.lineSeparator() + "ERROR > " + e.getMessage());
            System.out.print(ansi().reset());

            return 0;
        }
    }



    private static final String getPollsQuery
            = "SELECT * FROM Polls";
    private static final String getOptnsQuery
            = "SELECT * FROM Options";

    @SuppressWarnings("unchecked")
    public String getPolls()
    {
        var pollsArray = new JSONArray();
        try (var statement = connection.createStatement()) {
            ResultSet resultSet = statement.executeQuery(getPollsQuery);

            while (resultSet.next()) {
                JSONObject poll = new JSONObject();

                poll.put("Name", resultSet.getString("Name"));
                poll.put("Type", resultSet.getString("Type"));
                poll.put("BegDate", resultSet.getTimestamp("BegDate").toString());
                poll.put("EndDate", resultSet.getTimestamp("EndDate").toString());
                poll.put("State", resultSet.getString("State"));

                pollsArray.add(poll);
            }
        } catch (SQLException e) {
            System.out.print(ansi().fg(RED));
            System.out.println("ERROR > " + e.getMessage());
            System.out.print(ansi().reset());
        }
        return pollsArray.toJSONString();
    }

    @SuppressWarnings("unchecked")
    public String getOptns()
    {
        var optnsArray = new JSONArray();
        try (var statement = connection.createStatement()) {
            ResultSet resultSet = statement.executeQuery(getOptnsQuery);

            while (resultSet.next()) {
                JSONObject optn = new JSONObject();

                optn.put("PollName", resultSet.getString("PollName"));
                optn.put("Data", resultSet.getString("Data"));
                optn.put("Type", resultSet.getString("Type"));
                optn.put("VotesCount", Integer.toString(resultSet.getInt("VotesCount")));

                optnsArray.add(optn);
            }
        } catch (SQLException e) {
            System.out.print(ansi().fg(RED));
            System.out.println("ERROR > " + e.getMessage());
            System.out.print(ansi().reset());
        }
        return optnsArray.toJSONString();
    }
}
