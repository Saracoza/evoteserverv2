package util;

public class CloseSocketLock
{
    @SuppressWarnings("PointlessBooleanExpression")
    public static synchronized void closeSocket(java.net.Socket socket)
    {
        try {
            if (socket.isClosed() == false) {
                socket.close();
                logging.LogManager.getInstance().logClosedConnection(
                        socket.getInetAddress().getHostAddress(),
                        socket.getPort()
                );
            }
        } catch (java.io.IOException ignored) {}
    }
}
