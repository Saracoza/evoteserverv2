package util;

import java.net.Socket;
import java.util.LinkedList;

public class RunningState
{
    private volatile boolean running = true;
    public  boolean isRunning()
    {
        return running;
    }



    private LinkedList<Socket> sockets = new LinkedList<>();

    public void addSocket(java.net.Socket socket)
    {
        sockets.add(socket);
    }



    public void shutDown() throws java.io.IOException
    {
        running = false;

        var dummySocket = new java.net.Socket("localhost", Integer.parseInt(conf.SettingsChain.getInstance().getSettingValue("serverPort")));
        var dummyStream = new java.io.DataOutputStream(dummySocket.getOutputStream());
        dummyStream.writeInt(4);
        dummyStream.write("exit".getBytes());
        dummyStream.close();
    }



    public void closeSockets()
    {
        for (var socket : sockets) {
            CloseSocketLock.closeSocket(socket);
        }
    }
}
