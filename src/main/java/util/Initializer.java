package util;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import static org.fusesource.jansi.Ansi.*;
import static org.fusesource.jansi.Ansi.Color.*;

public class Initializer
{
    @SuppressWarnings("unchecked")
    public static void LoadComponents() throws Exception
    {
        util.Helper.createInstance("help.txt");

        data.DataAccess.createInstance();
        logging.LogManager.createInstance();
        core.CliShellExecutor.createInstance();
        core.InspectionHelper.createInstance();
        uprove.storage.AuthoritySettings.createInstance();



        var jsonParser = new JSONParser();
        var pollsArray = (JSONArray) jsonParser.parse(data.DataAccess.getInstance().getPolls());
        var optnsArray = (JSONArray) jsonParser.parse(data.DataAccess.getInstance().getOptns());



        var dateFormat = new java.text.SimpleDateFormat("yyyy-MM-dd hh:mm:ss.SSS");
        pollsArray.forEach(object -> {
            var poll = (JSONObject) object;
            try {
                electoral.PollCenter.getInstance().addPoll(
                        new electoral.Poll(
                                (String) poll.get("Name"),
                                electoral.PollType.valueOf((String) poll.get("Type")),
                                dateFormat.parse((String) poll.get("BegDate")),
                                dateFormat.parse((String) poll.get("EndDate")),
                                electoral.PollState.valueOf((String) poll.get("State"))
                        )
                );
            } catch (Exception e) {
                System.out.print(ansi().fg(RED));
                System.out.println("ERROR > " + e.getMessage());
                System.out.print(ansi().reset());
            }
        });



        optnsArray.forEach(object -> {
            try {
                var optn = (JSONObject) object;
                var data = (JSONObject) jsonParser.parse((String) optn.get("Data"));

                electoral.Option option = null;
                switch ((String) optn.get("Type")) {
                    case "A": {
                        option = new electoral.Answer(
                                (String) data.get("Text"),
                                (String) data.get("Info"));
                    } break;
                    case "C": {
                        option = new electoral.Candidate(
                                (String) data.get("NameF"),
                                (String) data.get("NameL"),
                                (String) data.get("Affiliation"));
                    } break;
                }

                if (option != null) {
                    option.setVotesCount(Integer.parseInt((String) optn.get("VotesCount")));

                    electoral.PollCenter.getInstance().getPoll((String) optn.get("PollName")).addOption(option, true);
                }
            } catch (Exception e) {
                System.out.print(ansi().fg(RED));
                System.out.println("ERROR > " + e.getMessage());
                System.out.print(ansi().reset());
            }
        });
    }
}
